from flask import Flask, render_template, request,jsonify
import os

home_path ='J://calculation'
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.route('/services')
def services():
    return render_template('services.html')
@app.route('/thank_you')
def thanks():
    return render_template('thank_you.html')

@app.route('/submit', methods=['POST'])
def submit():
    data = request.get_json()
    Bulkhead = data.get('bulkhead')
    WDb =  data.get('wDb')
    sheetingD=  data.get('sheetingD')
    pileD = data.get('pileD')
    tidalR=  data.get('tidalR')
    stormS =  data.get('stormS')
    rainF =  data.get('rainF')
    soil =  data.get('soil')
    email =  data.get('email')
    print('teststtttttttttttttt')
    # You can add your processing logic here, like saving to a database or sending an email.
    if (data):

        
        with open(os.path.join(home_path, "main.tex"), "r") as f:
            latex_content = f.read()

        # Define the specific values

        # Update the assumptions section with specific values
        assumptions_section = f"""
        \\section{{Assumptions}}

        \\begin{{multicols}}{{2}}
        \\noindent\\textbf{{Design specific:}}\\\\
        Bulkhead height above mean waterline: "{Bulkhead} m"\\\\
        Water depth at bulkhead: "{WDb} m"\\\\
        Sheeting depth: "{sheetingD} m"\\\\
        Pile depth: "{pileD} m"

        \\columnbreak
        \\noindent\\textbf{{Location specific:}}\\\\
        Tidal range: "$\\pm${tidalR} m solve for minimum depth"\\\\
        Storm surge max: "{stormS} m"\\\\
        Rainfall max: "{rainF} feet"\\\\
        Soil: {soil}
        \\end{{multicols}}
        """

        # Replace the old assumptions section with the updated one in the LaTeX content
        start_marker = "\\section{Assumptions}"
        end_marker = "\\section{Analysis}"
        start_index = latex_content.find(start_marker)
        end_index = latex_content.find(end_marker)
        latex_content = latex_content[:start_index] + assumptions_section + latex_content[end_index:]

        # Write the updated content back to the file
        with open(os.path.join(home_path, "main.tex"), "w") as f:
            f.write(latex_content)

        
        import calculator
        from calculator import send_email
        from calculator import convert_latex_to_pdf
        calculator    

                
        tex_file_path = os.path.join(home_path,"main.tex")  # Replace with the actual path to your '.tex' file
        convert_latex_to_pdf(tex_file_path)
            
        sender_email = 'hadguhadgu9@gmail.com'
        recipient_email = email  # Provide a valid recipient email address
        subject = 'BeachEngineer Calculation'
        body = """
        <html>
        <body>
            <h1>BeachEngineer Calculation</h1>
            <h2>generated form calculations by JhonBrindley with cal review byBeach Engineer GPT</h2>
            <p>Hello Dear,</p>
            <p>This email contains PDF results of the engineering calculations based on your needs</p>
            <p>Based </p>
            <ul>
            <li>Design specific</li>
            <li>Location specific</li>
            <li>check the PDF and contact us for more information.</li>
            
            </ul>
            <p>Thank you for your attention.</p>
            <p>Best regards,</p>
            <p>BeachEngineer teams</p>
        </body>
        </html>
        """
        file_path = os.path.join(home_path,'main.pdf')


    send_email(sender_email, recipient_email, subject, body,file_path)


    
    return jsonify({"thank_you_url": "/thank_you"})

    

if __name__ == '__main__':
    app.run(debug=True)
 